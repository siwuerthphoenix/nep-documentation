.. _nep-module:


************************
 NetEye Extension Packs
************************

NetEye Extension Packs (NEP) are a way to deliver the full experience of Wuerth Phoenix's NetEye to end users by employing a modular design. It allows fast startup for a brand new NetEye Infrastructure, and easy integration with an existing NetEye deployment, adding only the required features and components to system configuration.

.. toctree::
    :maxdepth: 2

    doc/nep-introduction.rst
    doc/getting-started/nep-getting-started.rst
    doc/nep-repository.rst
    doc/nep-obtaining-nep.rst
    doc/nep-insights.rst
    doc/nep-available-packages.rst
    doc/nep-advanced.rst
    doc/nep-updates.rst
