# NetEye Extension Packs - Documentation (NEP)

NetEye Extension Packs (NEP) are a way to deliver the full experience of Wuerth Phoenix’s NetEye to end users by employing a modular design. It allows fast startup for a brand new NetEye Infrastructure, and easy integration with an existing NetEye deployment, adding only the required features and components to system configuration.

This repo provide all the documentation wrote for NEP.

# For more details referer to [NetEye Guide](https://neteye.guide/current/nep.html)

# For Issues and Road Map [Jira Project](https://siwuerthphoenix.atlassian.net/jira/dashboards/10903)