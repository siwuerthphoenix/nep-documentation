.. _nep-online-resources:

******************
 Online Resources
******************

NetEye Extension Package offers two online resources for development and support:

* NEP's Jira Project Dashboard
* Wuerth Phoenix's Anonymous BitBucket Repository

These resources are available to help the End User in:

* Have a better understanding of the roadmap of the NEP Project
* Get early access to new (unreleased) features
* Actively participating in NEP Development by submitting bug segnalation and feature requests, as well as its own code

Therefore, all the NetEye NEP Project Contents are available online, under GPL Licence.


NEP's Jira Project Dashboard
============================
All the task and issues related to NetEye Extension Pack can be freely accessed through the following links:

- `Dashboard <https://siwuerthphoenix.atlassian.net/jira/dashboards/10903>`_
- `Unreleased issues <https://siwuerthphoenix.atlassian.net/issues/?filter=16286>`_
- `Released issues <https://siwuerthphoenix.atlassian.net/issues/?filter=16287>`_


Wuerth Phoenix's Anonymous BitBucket Repository
===============================================
NetEye Extension Pack complete code and contents are available at our Anonymous BitBucket Repository.
To easily get all the contents, just select the desired branch (or just keep on the ``master`` branch) and download the whole project as a ZIP File.
If you like to work with GitHub (or just use the ``git`` utility), clone the desider branch on yhour system.

To access the BitBucket Repository, please refer to the following URL:

`https://bitbucket.org/siwuerthphoenix/nep/src/master/ <https://bitbucket.org/siwuerthphoenix/nep/src/master/>`_