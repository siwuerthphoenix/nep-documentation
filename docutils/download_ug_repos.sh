cd
git clone https://bitbucket.org/siwuerthphoenix/neteye-userguide-content.git
cd neteye-userguide-content
git submodule update --init

# This is done to prevent missing variables on RPM spec files
cd continuous-integration
git checkout master
git pull
cd ..

# This is done to ensure no files are missing before building
cd troubleshooting-userguide
git checkout master
git pull
cd ..

# Select the branch that has to be tested
cd nep-documentation
git checkout master
git pull
cd ..