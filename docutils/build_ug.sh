#! /bin/bash
pushd /root/neteye-userguide-content
rm -rf RPMS/x86_64/*rpm
sh ./build_dist.sh
sh ./build_rpm.sh
yum install RPMS/x86_64/*rpm -y
/usr/local/bin/build_neteye_userguide.sh
popd
